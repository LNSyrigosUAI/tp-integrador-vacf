using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Objetivo : MonoBehaviour
{
    public float EstadoVital = 100f;
    public event Action EstatuaDerrotada;//GM

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void RecibirDaņo(float Cantidad)
    {
        EstadoVital -= Cantidad;

        if (EstadoVital <= 0f)
        {
            EstatuaDerrotada?.Invoke();//GM
            Destroy(gameObject);
        }
    }
}
