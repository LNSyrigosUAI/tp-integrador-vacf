using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    //TODO: Tiene que identificar que puertas estan completadas.
    //TODO: Una vez completado el nivel tiene que transferir al jugador a la escena del pantheon, pero conservando la informacion de que completo ese nivel.

    public GameObject Estatua;
    public string SiguienteEscenario;

    void Start()
    {
        Estatua.GetComponent<Control_Objetivo>().EstatuaDerrotada += HandleDestruirEstatua;
    }

    
    void Update()
    {
        
    }

    void HandleDestruirEstatua()
    {
        SceneManager.LoadScene(SiguienteEscenario);
    }

    private void OnDestroy()
    {
        if (Estatua != null)
        {
            Estatua.GetComponent<Control_Objetivo>().EstatuaDerrotada -= HandleDestruirEstatua;
        }
    }
}
