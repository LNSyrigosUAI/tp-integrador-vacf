using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Control_Acceso_Puerta : MonoBehaviour
{

    //TODO: Tiene que dar acceso a la escena donde transcurre el desafio.

    public string EscenaEstablecida;

    void OnMouseDown()
    {
        SceneManager.LoadScene(EscenaEstablecida);
    }
}
